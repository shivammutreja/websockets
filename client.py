import socket

def main():
    host = '127.0.0.1'
    port = 5001

    mySocket = socket.socket()
    mySocket.connect((host, port))

    message = input(" ? ")

    while message != 'q':
        while message != 'Bunty':
            message = input(" ? ")
        if message == "Bunty":
            mySocket.send(message.encode())
        data = mySocket.recv(1024).decode()

        print ('Received from server: ' + data)
        message = input(" ? ")

    mySocket.close()

if __name__ == "__main__":
    main()
	
	
