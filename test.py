from sanic import Sanic
import sanic.response as response
from websockets.exceptions import ConnectionClosed

app = Sanic()


@app.route('/')
async def index(request):
    return await response.file('index.html')


@app.websocket('/wsgate')
async def feed(request, ws):
    while True:
        try:
            data = await ws.recv()
        except (ConnectionClosed):
            print("Connection is Closed")
            data = None
            break
        print('Received: ' + data)
        res = "Got the msg"
        await ws.send(res)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8000, debug=True)


